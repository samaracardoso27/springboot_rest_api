package com.websystique.springboot;


import com.websystique.springboot.model.PagadorEletronico;
import org.springframework.web.client.RestTemplate;

public class SpringBootRestTestClient {

	public static final String REST_SERVICE_URI = "http://localhost:9090/cash_management/v1";



	/* GET */
	private static void getUser() {
		System.out.println("Testing getUser API----------");
		RestTemplate restTemplate = new RestTemplate();
		PagadorEletronico user = restTemplate.getForObject(REST_SERVICE_URI + "/pagadores_eletronicos/1",
				PagadorEletronico.class);
		System.out.println(user);
	}

	public static void main(String args[]) {
		getUser();
	}
}