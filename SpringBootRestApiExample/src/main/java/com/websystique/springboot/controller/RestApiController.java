package com.websystique.springboot.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.websystique.springboot.model.PagadorEletronico;
import com.websystique.springboot.service.PagadorEletronicoService;
import com.websystique.springboot.util.CustomErrorType;

@RestController
@RequestMapping("/cash_management/v1")
public class RestApiController {

	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

	@Autowired
	PagadorEletronicoService PagadorEletronicoService; //Service which will do all data retrieval/manipulation work


	@RequestMapping(value = "/pagadores_eletronicos/{id_pagador_eletronico}", method = RequestMethod.GET)
	public ResponseEntity<?> getUser(@PathVariable("id_pagador_eletronico") long id_pagador_eletronico,
			@RequestParam(value = "agencia_beneficario", required=false) String agencia,
			@RequestParam(value = "conta_beneficario" , required=false) String conta,
			@RequestParam(value = "dac_beneficario" , required=false) String dac
			) {
		logger.info("Fetching User with id {}", id_pagador_eletronico);
		logger.info("Fetching User with cpf {}", agencia);
		PagadorEletronico user = PagadorEletronicoService.findById(id_pagador_eletronico);
		if (user == null) {
			logger.error("User with id {} not found.", id_pagador_eletronico);
			ResponseEntity<?> responseEntity = new ResponseEntity<Object>(new CustomErrorType("User with id " + id_pagador_eletronico 
					+ " not found"), HttpStatus.NOT_FOUND);
			return responseEntity;
		}
		return new ResponseEntity<PagadorEletronico>(user, HttpStatus.OK);
	}

}