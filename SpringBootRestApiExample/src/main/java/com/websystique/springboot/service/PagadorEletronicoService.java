package com.websystique.springboot.service;

import com.websystique.springboot.model.PagadorEletronico;

public interface PagadorEletronicoService {

	PagadorEletronico findById(long id);

}
