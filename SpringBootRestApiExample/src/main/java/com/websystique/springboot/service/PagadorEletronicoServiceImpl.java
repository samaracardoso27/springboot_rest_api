package com.websystique.springboot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;

import com.websystique.springboot.model.PagadorEletronico;

@Service("PagadorEletronicoService")
public class PagadorEletronicoServiceImpl implements PagadorEletronicoService {

	private static final AtomicLong counter = new AtomicLong();

	private static List<PagadorEletronico> pagadores;
	
	static{
		pagadores = populateDummyUsers();
	}

	public PagadorEletronico findById(long id) {
		for (PagadorEletronico pagador : pagadores) {
			if (pagador.getId_pagador_eletronico()== id) {
				return pagador;
			}
		}
		return null;
	}

	private static List<PagadorEletronico> populateDummyUsers() {
		List<PagadorEletronico> users = new ArrayList<PagadorEletronico>();
		users.add(new PagadorEletronico(counter.incrementAndGet(), "boleto", "boleto"));
		users.add(new PagadorEletronico(counter.incrementAndGet(), "tef", ""));
		users.add(new PagadorEletronico(counter.incrementAndGet(), "boleto", ""));
		users.add(new PagadorEletronico(counter.incrementAndGet(), "boleto", ""));
		users.add(new PagadorEletronico(counter.incrementAndGet(), "boleto", ""));
		return users;
	}

}
