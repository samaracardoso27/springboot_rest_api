package com.websystique.springboot.model;

public class PagadorEletronico {

	private long id_pagador_eletronico;
	private String sugestao;
	private String cobranca;

	public PagadorEletronico(long id_pagador_eletronico, String sugestao,
			String string) {
		super();
		this.id_pagador_eletronico = id_pagador_eletronico;
		this.sugestao = sugestao;
		this.cobranca = string;
	}

	public long getId_pagador_eletronico() {
		return id_pagador_eletronico;
	}

	public void setId_pagador_eletronico(long id_pagador_eletronico) {
		this.id_pagador_eletronico = id_pagador_eletronico;
	}

	public String getSugestao() {
		return sugestao;
	}

	public void setSugestao(String sugestao) {
		this.sugestao = sugestao;
	}

	public String getCobranca() {
		return cobranca;
	}

	public void setCobranca(String cobranca) {
		this.cobranca = cobranca;
	}

	@Override
	public String toString() {
		return "PagadorEletronico [id_pagador_eletronico="
				+ id_pagador_eletronico + ", sugestao=" + sugestao
				+ ", cobranca=" + cobranca + "]";
	}

}
